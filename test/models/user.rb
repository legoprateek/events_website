# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string
#  email           :string
#  password        :string
#  gender          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  password_digest :string
#

class User < ActiveRecord::Base
	validates :name,  presence: true, length: { maximum: 50 }
  	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  	validates :email, 	presence: true, 
  						length: { maximum: 255 },
                    	format: { 
                    		with: VALID_EMAIL_REGEX,
                    		message: "Please enter email in the correct format" 
                    			},
                    	uniqueness: { 
                    		case_sensitive: false
                    				}
	
    validates :password, length: { minimum: 4 }
    validates :gender, presence: true
    has_secure_password

    has_many :event_attendances
    has_many :events, through: :event_attendances
end
