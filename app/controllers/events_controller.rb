class EventsController < ApplicationController

  def new
  end
  
  def index
    @events = Event.all.order(created_at: :desc)
  end


  def show
  	@event = Event.find(params[:id])
  	@user = current_user
  	@all_attendies = User.joins(:user_and_events).where('user_and_events.event_id = ?', params[:id])
  
  	if logged_in?
    	@user_event = User.joins(:user_and_events).where('user_and_events.event_id = ? AND user_and_events.user_id = ?', params[:id], current_user.id)
    end  
  end

  def createnewevent
    @temp = UserAndEvent.new(:user_id => current_user.id, :event_id => params[:id])
    @temp.save 
     redirect_to event_path(:id => params[:id])
   
  end
  
  def destroy
    UserAndEvent.where("user_id = ? AND event_id = ?", current_user.id, params[:id]).first.delete
    redirect_to :action => 'show' 
  end
  
 private
  	def past_event?(event_dt)
  	    date = Date.parse event_dt
  	    date.past?
  	end

  	helper_method :past_event?
end
