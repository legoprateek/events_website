# == Schema Information
#
# Table name: events
#
#  id            :integer          not null, primary key
#  name          :string
#  date_of_event :datetime
#  fee           :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Event < ActiveRecord::Base
  has_many :user_and_events
	has_many :users, through: :user_and_events
end
